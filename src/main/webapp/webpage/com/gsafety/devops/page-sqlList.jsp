<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<div style="margin: 0 15px;padding-bottom:10px;">
<!-- 若页面内容太多需要滚动条 ,可自己根据页面添加overflow: auto;样式 -->
<div class="conc-wrapper" style="margin-top:10px">
	<div class="main-tab">
		<ul class="tab-info">
		  <li role="presentation" class="active">
			<a href="javascript:void(0);"> &nbsp;&nbsp;信息模块1</a>
		  </li>
		</ul>
		<!-- tab内容 -->
		<div class="con-wrapper1">
		  <div class="row form-wrapper">
		<div class="row show-grid">
		<div class="col-xs-1 text-center">
			<b>SQL描述：</b>
		</div>
         <div class="col-xs-2">
			<input name="sqlName" value = "${sqlListPage.sqlName}" style="width:150px" maxlength="100" type="text" class="form-control"   ignore="ignore" />
			<span class="Validform_checktip" style="float:left;height:0px;"></span>
			<label class="Validform_label" style="display: none">SQL描述</label>
		</div>
		<div class="col-xs-1 text-center">
			<b>SQL类型：</b>
		</div>
         <div class="col-xs-2">
			<%-- <input name="sqlType" value = "${sqlListPage.sqlType}" style="width:150px" maxlength="6" type="text" class="form-control"   datatype="*"  ignore="checked" /> --%>
			<t:dictSelect field="sqlType" type="select" typeGroupCode="sql_type" hasLabel="false" title="SQL类型" defaultVal="Normal"></t:dictSelect>
			<span class="Validform_checktip" style="float:left;height:0px;"></span>
			<label class="Validform_label" style="display: none">SQL类型</label>
		</div>
			</div>
			</div>
		</div>
	</div>
	<div class="main-tab">
		<ul class="tab-info">
		  <li role="presentation" class="active">
			<a href="javascript:void(0);"> &nbsp;&nbsp;其他信息1</a>
		  </li>
		</ul>
		<!-- tab内容 -->
		<div class="con-wrapper1">
		  <div class="row form-wrapper">
		  <div class="row show-grid">
		
		<div class="col-xs-1 text-center">
			<b>SQL内容：</b>
		</div>
		<div class="col-xs-5">
				<textarea id="sqlContent" class="form-control" rows="16" name="sqlContent" cols="180"  datatype="*"  ignore="checked" >${sqlListPage.sqlContent}</textarea>
			<span class="Validform_checktip" style="float:left;height:0px;"></span>
			<label class="Validform_label" style="display: none">SQL内容</label>
		</div>
			</div></div></div></div>
	
</div>
</div>
<script type="text/javascript">
   $(function(){
	    //查看模式情况下,删除和上传附件功能禁止使用
		if(location.href.indexOf("load=detail")!=-1){
			$(".jeecgDetail").hide();
			$("input,select,textarea").attr("disabled","disabled");
		}
   });
</script>